import os
import itertools
from datetime import datetime,timezone,timedelta

os.system('rm -rf innerrunbash/qrdqn_gsgd_adam')
os.mkdir('innerrunbash/qrdqn_gsgd_adam')
os.chdir('innerrunbash/qrdqn_gsgd_adam')

def name(job):
    return('-'.join([str(x) for x in job.values() ]))

def genreate(
        cuda,
        game,
        opt,
        inner_opt,
        ec_opt,
        ec_layer_type,
        ec_adam_method,
        log_name,
        idx,
        setting,
        dir='../..',

        ):

    log_name = '-'.join([opt,inner_opt,ec_opt,log_name,str(idx)])
    templete = '''
    p=$(dirname $0)
    cd $p/{dir}/    
    
    

     python examples.py  \
     --cuda {cuda} \
     --game {game}  \
     --opt {opt} \
     --inner_opt {inner_opt} \
     --ec_opt {ec_opt} \
     --ec_layer_type {ec_layer_type} \
     --ec_adam_method {ec_adam_method} \
     --log_name {log_name} \
     &
     sleep 1s


        '''
    # print(cuda, setting)

    return templete.format(
        cuda=cuda,
        game=game,
        dir = dir,

        opt=opt,
        inner_opt=inner_opt,
        ec_opt=ec_opt,
        ec_layer_type=ec_layer_type,
        ec_adam_method=ec_adam_method,
        log_name=log_name,



    )


def check_right(all_config):
    res = []

    for ii in all_config:
        # lr = ii['lr']
        opt = ii['opt']
        inner_opt = ii['inner_opt']
        ec_opt = ii['ec_opt']
        ec_layer_type = ii['ec_layer_type']
        if opt == 'sgd' and (inner_opt != 'sgd' or ec_opt != 'bp'
                             or ec_layer_type != 2):
            continue
        if opt == 'adam' and (inner_opt != 'adam' or ec_opt != 'bp'
                              or ec_layer_type != 2):
            continue
        if opt == 'rmsprop' and (inner_opt != 'rmsprop' or ec_opt != 'bp'
                              or ec_layer_type != 2):
            continue


        # if opt == 'gsgd' and (inner_opt != 'sgd' or ec_opt != 'ec') :
        #     continue
        # if opt == 'gsgd_adam' and (inner_opt != 'adam' or ec_opt != 'ec'):
        #     continue
        # if opt == 'gsgd_rmsprop' and (inner_opt != 'rmsprop' or  ec_opt != 'ec') :
        #     continue

        if opt == 'gsgd' and (inner_opt != 'sgd'  ) :
            continue
        if opt == 'gsgd_adam' and (inner_opt != 'adam'  ):
            continue
        if opt == 'gsgd_rmsprop' and (inner_opt != 'rmsprop' ) :
            continue

        res.append(ii)
    return res

def generate_config(**kwargs):
    all_keys = list(kwargs.keys())
    all_values = list(kwargs.values())
    all_res_keys = [x[:-1] for x in all_keys]
    res = itertools.product(*all_values)
    res = [dict(zip(all_res_keys, x)) for nx, x in enumerate(res)]
    for nx, x in enumerate(res):
        x['setting'] = nx
    return (res)

def get_time_str():
    dt = datetime.utcnow()
    dt = dt.replace(tzinfo=timezone.utc)
    tzutc_8 = timezone(timedelta(hours=8))
    local_dt = dt.astimezone(tzutc_8)

    local_dt = local_dt.strftime("%y%m%d")
    return  local_dt

games =  ['air_raid', 'alien', 'amidar', 'assault', 'asterix', 'asteroids', 'atlantis',
    'bank_heist', 'battle_zone', 'beam_rider', 'berzerk', 'bowling', 'boxing', 'breakout', 'carnival',
    'centipede', 'chopper_command', 'crazy_climber', 'demon_attack', 'double_dunk',
    'elevator_action', 'enduro', 'fishing_derby', 'freeway', 'frostbite', 'gopher', 'gravitar',
    'hero', 'ice_hockey', 'jamesbond', 'journey_escape', 'kangaroo', 'krull', 'kung_fu_master',
    'montezuma_revenge', 'ms_pacman', 'name_this_game', 'phoenix', 'pitfall', 'pong', 'pooyan',
    'private_eye', 'qbert', 'riverraid', 'road_runner', 'robotank', 'seaquest', 'skiing',
    'solaris', 'space_invaders', 'star_gunner', 'tennis', 'time_pilot', 'tutankham', 'up_n_down',
    'venture', 'video_pinball', 'wizard_of_wor', 'yars_revenge', 'zaxxon']


gym_games = [''.join([g.capitalize()  for g in game.split('_')])+'NoFrameskip-v4'    for game in games ]

# gym_games = gym_games[0:1]

idxs=list(range(3))

opts=[ 'adam', 'gsgd_adam']
ec_opts = ['ec','bp']
inner_opts = [  'adam']
ec_layer_types = [2]
ec_adam_methods = ['noweight']
log_names = [get_time_str()]


cudas = [0]

config_pool ={
    'games' :gym_games,
    'idxs' : idxs,
    'cudas' : cudas,
    'opts' : opts,
    "inner_opts": inner_opts,
    "ec_opts": ec_opts,
    "ec_layer_types": ec_layer_types,
    "ec_adam_methods": ec_adam_methods,
    "log_names" : log_names


}
all_res = generate_config(**config_pool)

all_res = check_right(all_res)


class bash_generator():
    def __init__(self):
        self.gpu_count = 0
        self.all_job_num = 0
        self.job_log = open('job_log.csv', 'w')
        self.job_num = 0

        self.set_config()
        self.init_bash()
        self.init_fp()
        self.init_joblog()

    def add_one(self, job):
        self.all_job_num += 1
        job_config_string = name(job)
        print(self.gpu_count, job['cuda'])
        self.job_log.write(',{},{}\n'.format(self.all_job_num, job_config_string))
        job['cuda'] = self.gpu_count
        self.bash.append(genreate(**job))


        self.gpu_count += 1

        if self.gpu_count == self.card_per_job:
            self.write_fp()
            self.init_joblog()
            self.init_bash()
            self.init_fp()
            self.gpu_count = 0

    def set_config(self, card_per_job=4):

        self.card_per_job = card_per_job


    def init_bash(self):
        self.bash = []
        self.dir_tmp = []
        # self.bash.append('cd /philly/eu2/pnrsy/v-yuewng/project/philly-dqn-baseline/dqn_baseline')
        # self.bash.append('nvidia-smi --loop=2 &')

    def init_fp(self):
        self.fp = open('%d_job_%d.sh' % (self.job_num, self.all_job_num), 'w')
        self.job_num += 1

    def init_joblog(self):
        self.job_log.write('job_%d, \n' % (self.all_job_num))

    def write_fp(self):
        if self.bash:
            self.fp.write('\n'.join(self.bash))
            self.fp.write('\n wait')
        self.fp.close()

    def close_joblog(self):
        self.job_log.close()


a = bash_generator()
[a.add_one(x) for x in all_res]
a.write_fp()
a.close_joblog()

